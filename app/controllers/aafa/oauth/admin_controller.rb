class Aafa::Oauth::AdminController < AafaController
  http_basic_authenticate_with :name => Aafa.admin_dashboard_username, :password => Aafa.admin_dashboard_password

  def new
    @client_app = Aafa::Oauth::ClientApp.new
  end

  # Show all client applications belonging to the current user
  def index
    @client_apps = Aafa::Oauth::ClientApp.all
    @all_apps_count = Aafa::Oauth::ClientApp.count
  end

  def show
    @client_app = client_app
  end

  def edit
    @client_app = client_app
  end

  def update
    @client_app = client_app
    @client_app.name = params[:aafa_oauth_client_app][:name]
    @client_app.link = params[:aafa_oauth_client_app][:link]
    @client_app.mini_icon_link = params[:aafa_oauth_client_app][:mini_icon_link]
    @client_app.small_icon_link = params[:aafa_oauth_client_app][:small_icon_link]
    @client_app.icon_link = params[:aafa_oauth_client_app][:icon_link]
    if @client_app.save
      redirect_to api_admin_path(@client_app)
    else
      render :edit
    end
  end

  def create
    @client_app = Aafa::Oauth::ClientApp.create_with_name(params[:aafa_oauth_client_app][:name], 
      params[:aafa_oauth_client_app][:link], 
      params[:aafa_oauth_client_app][:mini_icon_link], 
      params[:aafa_oauth_client_app][:small_icon_link], 
      params[:aafa_oauth_client_app][:icon_link])
    
    if @client_app.save
      redirect_to api_admin_path(@client_app)
    else
      render :new
    end
  end

  def destroy
    @client_app = client_app
    @client_app.destroy
    redirect_to api_admin_index_path
  end

  def client_app
    Aafa::Oauth::ClientApp.find params[:id]
  end
end
