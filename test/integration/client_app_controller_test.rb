require 'test_helper'

class ClientAppControllerTest < ActiveSupport::IntegrationCase

  test 'create client application' do
    basic_auth('admin', 'GodLike1')
    visit new_api_admin_path
    
    assert_equal '/api/admin/new', current_path

    fill_in 'aafa_oauth_client_app_name', :with => rand_name

    click_button 'submitApp'
    assert_match /api\/admin\/\d*/, current_path

    last_client = Aafa::Oauth::ClientApp.order(:created_at).last
    assert has_content?(last_client.name)
    assert has_content?(last_client.client_id)
    assert has_content?(last_client.client_secret)
  end

  test 'edit existing client application' do
    basic_auth('admin', 'GodLike1')
    app = create_client_app
    visit api_admin_path(app)

    click_link "edit"

    assert_equal edit_api_admin_path(app), current_path
    new_name = rand_name
    old_name = app.name
    refute new_name == old_name # smoke test

    fill_in "aafa_oauth_client_app_name", :with => new_name
    click_button 'submitApp'

    assert has_content?(new_name)
  end

  test 'index client applications' do
    app = create_client_app
    create_client_app
    create_client_app
    basic_auth('admin', 'GodLike1')
    visit api_admin_index_path
    assert_equal api_admin_index_path, current_path
    assert !has_content?("You have no applications.")
  end

  test 'index client applications and no app found' do
    basic_auth('admin', 'GodLike1')
    visit api_admin_index_path
    assert_equal api_admin_index_path, current_path
    assert has_content?("You have no applications.")
  end

  test 'delete existing client application' do
    basic_auth('admin', 'GodLike1')
    app = create_client_app
    visit api_admin_index_path

    click_link 'delete'

    refute has_content?(app.name)
    assert_equal api_admin_index_path, current_path
  end

  private

  def basic_auth(name, password)
    if page.driver.respond_to?(:basic_auth)
      page.driver.basic_auth(name, password)
    elsif page.driver.respond_to?(:basic_authorize)
      page.driver.basic_authorize(name, password)
    elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
      page.driver.browser.basic_authorize(name, password)
    else
      raise "I don't know how to log in!"
    end
  end
end
