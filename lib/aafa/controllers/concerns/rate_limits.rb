module Aafa::Controllers::Concerns::RateLimits
  extend ActiveSupport::Concern

  included do
    before_filter :oauth_record_rate_limit!,  :if => :valid_oauth?
    before_filter :oauth_fail_limit!,       :if => :oauth_client_over_rate_limit?
  end

  def oauth_client_record_access!(client_id, params)
    # implement your access counting mechanism here
  end

  def oauth_client_rate_limited?(client_id, params)
    # implement your rate limiting algorithm here
  end


  # override to implement custom rate limits
  def oauth_client_over_rate_limit?
    return oauth_client_rate_limited?(oauth_client_app.id, params) if allow_oauth? and oauth_client_app.present?
    false
  end

  def oauth_record_rate_limit!
    return false if oauth_client_app.blank?
    oauth_client_record_access!(oauth_client_app.id, params)
  end

  def oauth_client_under_rate_limit?
    !oauth_client_over_rate_limit?
  end

  def oauth_fail_limit!
    render :json => {:error => {:message => 'Rate limit exceeded', :code => 105, :description => 'The request limit for this resource has been reached for the current rate limit window.'}}, :status => :unauthorized
    false
  end
end