require 'aafa/rails/routes'

module Aafa
  class Engine < Rails::Engine

    initializer "aafa.include_helpers" do
      Aafa.include_helpers(Aafa::Controllers)
    end
  end
end
