class UpdateClientAppsAddIcon < ActiveRecord::Migration
  def change    
    add_column :aafa_client_apps, :mini_icon_link, :string
    add_column :aafa_client_apps, :small_icon_link, :string
    add_column :aafa_client_apps, :icon_link, :string
  end
end