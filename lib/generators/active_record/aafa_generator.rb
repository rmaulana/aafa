require 'rails/generators/migration'


module ActiveRecord
  module Generators
    class Aafa < ::Rails::Generators::Base
      include Rails::Generators::Migration
      source_root File.expand_path('../templates', __FILE__)

      desc "add the migrations needed for aafa oauth"

      def self.next_migration_number(path)
        unless @prev_migration_nr
          @prev_migration_nr = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
        else
          @prev_migration_nr += 1
        end
        @prev_migration_nr.to_s
      end


      def copy_migrations
        migration_template "auth_grants.rb", "db/migrate/create_aafa_auth_grants.rb"
        migration_template "client_apps.rb", "db/migrate/create_aafa_client_apps.rb"
        migration_template "update_client_apps.rb", "db/migrate/update_aafa_client_apps.rb"
        migration_template "update_client_apps_add_icon.rb", "db/migrate/update_aafa_client_apps_add_icon.rb"
        migration_template "update_client_apps_add_user.rb", "db/migrate/update_client_apps_add_user.rb"
        migration_template "update_client_apps_add_user_index.rb", "db/migrate/update_client_apps_add_user_index.rb"
      end
    end
  end
end
